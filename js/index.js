ZoomMtg.setZoomJSLib('https://source.zoom.us/1.8.5/lib', '/av');


ZoomMtg.preLoadWasm();
ZoomMtg.prepareJssdk();

const zoomMeeting = document.getElementById("zmmtg-root")

ZoomMtg.init({
    leaveUrl: "https://example.com/return_url",
    success: () => {
        ZoomMtg.join({
            meetingNumber: "XXXXXXXXXX",
            userName: "test"
        })
    },
    error: function (res) {
        console.log(res);
    },
})